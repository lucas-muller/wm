#include<iostream>
#include<Windows.h>

#define UNUSED(expr) do { (void)(expr); } while (0)

HHOOK keyboard_hook;

LRESULT CALLBACK keyboard_proc(int code, WPARAM wParam, LPARAM lParam)
{
  UNUSED(wParam);
  UNUSED(lParam);

  if (wParam == WM_KEYDOWN)
  {
    KBDLLHOOKSTRUCT* kdb_struct = (KBDLLHOOKSTRUCT*)lParam;
    DWORD key_code = kdb_struct->vkCode;
    std::cout << "Key: " << (char)key_code << std::endl;

    if (key_code == (DWORD)'A') 
    {
      std::cerr << "skip" << std::endl;
      return 1;
    }
  }

  return CallNextHookEx(NULL, code, wParam, lParam);
}

int main()
{
  std::cout << "WM" << std::endl;

  keyboard_hook = SetWindowsHookExW(WH_KEYBOARD_LL, keyboard_proc, NULL, 0);

  MSG msg;
  while (GetMessage(&msg, NULL, 0, 0))
  {
    TranslateMessage(&msg);
    DispatchMessage(&msg);
  }

  UnhookWindowsHookEx(keyboard_hook);

  return 0;
}
